# CHANGELOG



## v0.1.1 (2024-04-07)

### Fix

* fix(deploy): add deploy package ([`5e68c23`](https://gitlab.com/nativinetz/my-package/-/commit/5e68c230a511dc0ea64baa36e8480fb4b5a2dc1a))

### Unknown

* fix(deploy) ([`8429cc3`](https://gitlab.com/nativinetz/my-package/-/commit/8429cc38b19211c9556c6cdfd29992ab4d61d6cc))


## v0.1.0 (2024-04-07)

### Feature

* feat(semantic-release): add semantic release to the repository ([`f029aaf`](https://gitlab.com/nativinetz/my-package/-/commit/f029aaff80881621de4967f22abbc2d0164de5b0))


## v0.0.0 (2024-04-07)

### Unknown

* Update .gitlab-ci.yml file, replaced content ([`4bae934`](https://gitlab.com/nativinetz/my-package/-/commit/4bae934f5652d54373e896d5056f01389f021276))

* Add semantic release config ([`00d652c`](https://gitlab.com/nativinetz/my-package/-/commit/00d652c2b0e226105e181db5eb9c91ea2471dd1d))

* Add new file pyproject.toml ([`91f4e0a`](https://gitlab.com/nativinetz/my-package/-/commit/91f4e0afcce8a607aedd9e8fa23b71bc06c505c2))

* Update .gitlab-ci.yml file ([`bb7ae92`](https://gitlab.com/nativinetz/my-package/-/commit/bb7ae92906ee0572de98552889c39d43cb8d3506))

* Add first pipeline ([`94b1bae`](https://gitlab.com/nativinetz/my-package/-/commit/94b1bae98207ab0cad2d8ca6d2ce3dcbd5b2c2a8))

* Initial commit ([`8807204`](https://gitlab.com/nativinetz/my-package/-/commit/8807204fdae8842865781bec30d51cef7be3a810))
